﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Drawing;

public class UtilHttp
{
    public static string UrlEncode(string str)
    {
        StringBuilder sb = new StringBuilder();
        byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
        for (int i = 0; i < byStr.Length; i++)
        {
            sb.Append(@"%" + Convert.ToString(byStr[i], 16));
        }

        return (sb.ToString());
    }

    public static string HttpGet(string URL)
    {
        // 创建HttpWebRequest对象
        HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(URL);
        httpRequest.Method = "GET";

        //设置https验证方式
        if (URL.StartsWith("https", StringComparison.OrdinalIgnoreCase))
        {
            ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(CheckValidationResult);
        }

        try
        {
            using (HttpWebResponse myResponse = (HttpWebResponse)httpRequest.GetResponse())
            {
                StreamReader sr = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                string responseString = sr.ReadToEnd();
                return responseString;
            }
        }
        catch (WebException ex)
        {
            var res = (HttpWebResponse)ex.Response;
            StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
            string str = sr.ReadToEnd();
            return str;
        }
    }

    public static string HttpPost(string URL, string Para)
    {
        // 创建HttpWebRequest对象
        HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(URL);
        httpRequest.Method = "POST";
        httpRequest.ContentType = "application/x-www-form-urlencoded";

        //设置https验证方式
        if (URL.StartsWith("https", StringComparison.OrdinalIgnoreCase))
        {
            ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(CheckValidationResult);
        }

        byte[] bytes = Encoding.UTF8.GetBytes(Para);

        using (Stream reqStream = httpRequest.GetRequestStream())
        {
            reqStream.Write(bytes, 0, bytes.Length);
            reqStream.Flush();
        }
        try
        {
            using (HttpWebResponse myResponse = (HttpWebResponse)httpRequest.GetResponse())
            {
                StreamReader sr = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                string responseString = sr.ReadToEnd();
                return responseString;
            }
        }
        catch (WebException ex)
        {
            var res = (HttpWebResponse)ex.Response;
            StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
            string str = sr.ReadToEnd();
            return str;
        }
    }

    public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
        //直接确认，否则打不开    
        return true;
    }

    #region Image To base64
    public static Image UrlToImage(string url)
    {
        WebClient mywebclient = new WebClient();
        byte[] Bytes = mywebclient.DownloadData(url);
        using (MemoryStream ms = new MemoryStream(Bytes))
        {
            Image outputImg = Image.FromStream(ms);
            return outputImg;
        }
    }
    /// <summary>
    /// Image 转成 base64
    /// </summary>
    /// <param name="fileFullName"></param>
    public static string ImageToBase64(Image img)
    {
        try
        {
            Bitmap bmp = new Bitmap(img);
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] arr = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(arr, 0, (int)ms.Length);
            ms.Close();
            return "data:image/png;base64," + Convert.ToBase64String(arr);
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public static string ImageToBase64(string url)
    {
        return ImageToBase64(UrlToImage(url));
    }
    #endregion
}
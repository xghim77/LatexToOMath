//引入 express 框架
const express = require("express");
 
// 实例化 express 
const app = express();
 
// 引入 url 模块
const url = require("url");
 
//引入 body-parser
const bodyParser = require("body-parser")
 
// 接受post参数
app.use(bodyParser.urlencoded({
    extended: true
}))
 
 
// 接受参数 user=?
app.get("/tex2mml", (req, res) => {
    var yourMath = url.parse(req.url, true).query.tex;
    var mjAPI = require("mathjax-node");
    mjAPI.config({
        MathJax: {
            // traditional MathJax configuration
        }
    });
    mjAPI.start();

    mjAPI.typeset({
        math: yourMath,
        format: "TeX", // or "inline-TeX", "MathML"
        mml:true,      // or svg:true, or html:true
    }, function (data) {
        if (!data.errors) {
            res.send(data.mml);
        }
        else
        {
            res.send("");
        }
        res.end(data);
    });

    //res.end();
})

app.get("/tex2svg", (req, res) => {
    var yourMath = url.parse(req.url, true).query.tex;
    require('mathjax').init({
        loader: {load: ['input/tex', 'output/svg']}
    }).then((MathJax) => {
        var svg = MathJax.tex2svg(yourMath, {display: true});
        res.send(MathJax.startup.adaptor.outerHTML(svg));
    }).catch((err) => console.log(err.message));
})



app.get("/TexToMml", (req, res) => {
    var yourMath = url.parse(req.url, true).query.tex;
    require('mathjax').init({
        loader: {load: ['input/tex', 'output/svg']}
    }).then((MathJax) => {
        const mml = MathJax.tex2mml(yourMath, {display: true});
        res.send(mml);
    }).catch((err) => console.log(err.message));
})
 
app.listen(3000, function() {
    console.log(3000);
})

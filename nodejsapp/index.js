//GET /test?name=lmw&tel=123456789

//app.get('/test', function(req, res) {
//    console.log(req.query.name);
//    console.log(req.query.tel);
//});

app.get('/', (req, res) => {
    //res.send('Hello World!')
    var mjAPI = require("mathjax-node");
    mjAPI.config({
        MathJax: {
            // traditional MathJax configuration
        }
    });
    mjAPI.start();

    var yourMath = req.query.tex;   //f(x)={x}^{2}+6x

    mjAPI.typeset({
        math: yourMath,
        format: "TeX", // or "inline-TeX", "MathML"
        mml:true,      // or svg:true, or html:true
    }, function (data) {
        if (!data.errors) {res.send(data.mml)}
    });
})
﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 图像长度单位转换
/// </summary>
public class ImageExtent
{
    private const decimal CM_TO_PX = 96M;
    private const decimal INCH_TO_CM = 2.54M;
    /// <summary>
    /// 厘米到EMU（English Metric Unit)
    /// </summary>
    private const decimal CM_TO_EMU = 360000M;

    /// <summary>
    /// EMU（English Metric Unit) 到像素（px）
    /// </summary>
    /// <param name="EMU"></param>
    public static decimal EMU_TO_PX(decimal EMU)
    {
        return EMU / CM_TO_EMU / INCH_TO_CM * CM_TO_PX;
    }
}
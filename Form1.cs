﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Ovml = DocumentFormat.OpenXml.Vml.Office;
using W = DocumentFormat.OpenXml.Wordprocessing;
using HtmlAgilityPack;
using System.Drawing.Imaging;

namespace TestOpenXmlW
{
    public partial class Form1 : Form
    {
        Dictionary<string, string> sDicPic = new Dictionary<string, string>();
        string strTmpDoc;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string strInfo = readTxt(Application.StartupPath + "\\使用说明.txt");
            rtbMsg.Text = strInfo;


            try
            {
                Directory.Delete(Application.StartupPath + "\\temp\\", true);
                }
            catch
            { } 

            try
            {
                Directory.CreateDirectory(Application.StartupPath + "\\temp\\");
            }
            catch
            { }



            //string sHtmlText = readTxt(Application.StartupPath + "\\test.html");

            //HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            //doc.LoadHtml(sHtmlText);

            //HtmlNodeCollection imgList = doc.DocumentNode.SelectNodes(".//img");

            //if (imgList != null)
            //{
            //    foreach (HtmlNode tmpImg in imgList)
            //    {
            //        HtmlAttribute latex = tmpImg.Attributes["data-latex"];
            //        Console.WriteLine(latex.Value);
            //    }
            //}
        }

        private string GetWebMathMml(string strLatex)
        {
            
            string result = "";

            // "http://localhost:3000/textomml?tex=" + strLatex; 
            //"http://localhost:3000/tex2mml?tex=" + strLatex;

            string strWebUrl = "http://192.168.0.231:3000/tex2mml?tex=" + UtilHttp.UrlEncode(strLatex);

            string strHtml = UtilHttp.HttpGet(strWebUrl);   //UtilHttp.HttpPost(strWebUrl, "tex=" + strLatex);
            //rtbMsg.AppendText("代码：" + strLatex + " 转换结果：" + strHtml);

            result = strHtml;


            //GetFinalHTML getHtml = new GetFinalHTML();
            //if (getHtml.Run(strWebUrl))
            //{
            //    strHtml = getHtml.HtmlBody;

                

            //    ////(<math[^>])([\s\S]*?)(</math>)

            //    //// 定义正则表达式用来匹配 math 标签 
            //    //Regex m_hvtRegImg = new Regex(@"(<math[^>])([\s\S]*?)(</math>)", RegexOptions.IgnoreCase);

            //    //// 搜索匹配的字符串 
            //    //MatchCollection matches = m_hvtRegImg.Matches(strHtml);

            //    //// 取得匹配项列表 
            //    //if (matches.Count > 0)
            //    //{
            //    //    result = matches[0].Value;
            //    //}
            //}

            return result;
        }

        /// <summary>
        /// html转word
        /// </summary>
        /// <param name="newFilePath">新docx</param>
        /// <param name="html">html</param>
        /// <returns></returns>
        public void HtmlToWord(string newFilePath, string html)
        {
            //加载word模板。
            Aspose.Words.Document wordDoc = new Aspose.Words.Document();
            Aspose.Words.DocumentBuilder builder = new Aspose.Words.DocumentBuilder(wordDoc);
            builder.Font.Name = "宋体";
            builder.InsertHtml(html, true);
            wordDoc.Save(newFilePath, Aspose.Words.Saving.SaveOptions.CreateSaveOptions(Aspose.Words.SaveFormat.Docx));
        }

        /// <summary>
        /// MathMl转换为Latex
        /// </summary>
        string ConvertToLatex(string sXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sXml);

            System.Xml.Xsl.XslCompiledTransform transform = new System.Xml.Xsl.XslCompiledTransform();
            transform.Load(Path.Combine(Application.StartupPath, "xsltran\\mmltex.xsl")); //编译后的样式表
            System.IO.StringWriter writer = new System.IO.StringWriter();
            XmlReader xmlReadB = new XmlTextReader(new StringReader(doc.DocumentElement.OuterXml));
            transform.Transform(xmlReadB, null, writer);


            return writer.ToString();
        }


        /// <summary>
        /// omath的OpenXml转换为MathMl
        /// </summary>
        private string ConvertToMMl(string sXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sXml);

            string result = "";
            using (var ms = new MemoryStream())
            {
                var xslTransform = new XslCompiledTransform();
                xslTransform.Load(Path.Combine(Application.StartupPath, "xsltran\\OMML2MML.XSL"));
                xslTransform.Transform(doc, XmlWriter.Create(ms));
                ms.Seek(0, SeekOrigin.Begin);

                StreamReader sr = new StreamReader(ms, Encoding.UTF8);
                result = sr.ReadToEnd();
            }
            return result;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string strLatex = txtCode.Text;
            if (string.IsNullOrEmpty(strLatex))
            {
                MessageBox.Show("请先输入Latex代码!");
                txtCode.Focus();
                return;
            }

            //rtbMsg.Clear();

            try
            {
                //ConvertEq p = new ConvertEq();
                //ConvertEquation ce = new ConvertEquation();

                //string strMathFile = p.GetOutputFile("txt");

                //ce.Convert(new EquationInputFileText2(strLatex, ClipboardFormats.cfTeX),
                //          new EquationOutputFileText(strMathFile, "MathJax-MathML.tdl"));

                //string strTmpXml = readTxt(strMathFile);

                string strTmpXml = GetWebMathMml(strLatex);

                //if (string.IsNullOrEmpty(strTmpXml) == false)
                //{
                //    strTmpXml = strTmpXml.Replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
                //}

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(strTmpXml);

                XslCompiledTransform xslTransform = new XslCompiledTransform();

                // The MML2OMML.xsl file is located under 
                // %ProgramFiles%\Microsoft Office\Office12\
                xslTransform.Load("xsltran\\MML2OMML.xsl");


                string strDocxFile = Application.StartupPath + "\\temp\\" + DateTime.Now.Ticks.ToString() + ".docx"; ;
                File.Copy("template.docx", strDocxFile);

                //[FIMG_001]

                // Load the file containing your MathML presentation markup.
                using (XmlReader reader = new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        XmlWriterSettings settings = xslTransform.OutputSettings.Clone();

                        // Configure xml writer to omit xml declaration.
                        settings.ConformanceLevel = ConformanceLevel.Fragment;
                        settings.OmitXmlDeclaration = true;

                        XmlWriter xw = XmlWriter.Create(ms, settings);

                        // Transform our MathML to OfficeMathML
                        xslTransform.Transform(reader, xw);
                        ms.Seek(0, SeekOrigin.Begin);

                        StreamReader sr = new StreamReader(ms, Encoding.UTF8);

                        string officeML = sr.ReadToEnd();

                        Console.Out.WriteLine(officeML);

                        // Create a OfficeMath instance from the  OfficeMathML xml.
                        DocumentFormat.OpenXml.Math.OfficeMath om =
                          new DocumentFormat.OpenXml.Math.OfficeMath(officeML);

                        // Add the OfficeMath instance to our word template.
                        using (WordprocessingDocument wordDoc =
                          WordprocessingDocument.Open(strDocxFile, true))
                        {

                            var body = wordDoc.MainDocumentPart.Document.Body;
                            var paras = body.Elements<Paragraph>();

                            foreach (var para in paras)
                            {
                                //[FIMG_001]
                                if (para.InnerText.Contains("[FIMG_001]"))
                                {
                                    //text.Text = text.Text.Replace("text-to-replace", "replaced-text");

                                    foreach (var currentRun in om.Descendants<DocumentFormat.OpenXml.Math.Run>())
                                    {
                                        // Add font information to every run.
                                        DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties2 =
                                          new DocumentFormat.OpenXml.Wordprocessing.RunProperties();

                                        RunFonts runFonts2 = new RunFonts() { Ascii = "Cambria Math", HighAnsi = "Cambria Math" };
                                        runProperties2.Append(runFonts2);

                                        currentRun.InsertAt(runProperties2, 0);
                                    }

                                    para.Append(om);
                                    //para.Parent.ReplaceChild(om, text);
                                }
                            }
                        }
                    }
                }

                //MessageBox.Show("转换完成!具体请查看"+ strDocxFile);
                AppendMsg("转换完成!具体请查看" + strDocxFile + "\n");

                Process p1 = new Process();
                p1.StartInfo.FileName = "explorer.exe";
                p1.StartInfo.Arguments = @" /open, " + strDocxFile;
                p1.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show(ex.ToString());
            }
        }

        void AppendMsg(string msg)
        {
            rtbMsg.AppendText(System.DateTime.Now.ToString() + " " + msg);
            rtbMsg.SelectionStart = rtbMsg.TextLength;
            rtbMsg.SelectionLength = 0;
            rtbMsg.ScrollToCaret();
            Application.DoEvents();
            Refresh();
        }

        /// <summary>
        /// 读取文本文件
        /// </summary>
        private string readTxt(string strTxtFile)
        {
            string result = "";
            if (File.Exists(strTxtFile))
            {
                result = File.ReadAllText(strTxtFile, Encoding.UTF8);
            }
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnOk.Enabled = false;
            button1.Enabled = false;
            pictureBox1.Visible = true;
            this.Cursor = Cursors.WaitCursor;
            
            //读取题库平台导出来的HTML文件，里面包含公式图片
            string sHtmlText = readTxt(Application.StartupPath + "\\test3.html");

            //<img\b[^<>]*?\bsrc[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>
            //<img\b[^>]*?\bdata-latex[\s]*=[\s]*["']?[\s]*(?<imgUrl>[^"'>]*)[^>]*?/?[\s]*>
            //<img[^<]*src="[^<]*"[^<]*>
            //<img\b[^<>]*?\bdata-latex[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>
            
            // 定义正则表达式用来匹配 img 标签 
            //Regex m_hvtRegImg = new Regex(@"<img\b[^<>]*?\b[^<>]*?/?[\s\t\r\n]*>", RegexOptions.Multiline | RegexOptions.IgnoreCase);

            // 搜索匹配的字符串 
            //MatchCollection matches = m_hvtRegImg.Matches(sHtmlText);

            int x = 0;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(sHtmlText);

            HtmlNodeCollection imgList = doc.DocumentNode.SelectNodes(".//img");

            if (imgList != null)
            {
                foreach (HtmlNode tmpImg in imgList)
                {
                    x++;

                    HtmlAttribute latex = tmpImg.Attributes["data-latex"];
                    //Console.WriteLine(latex.Value);
                    //\\hspace\{([^\{^\}]*)\}

                    string strTmpTex = latex.Value;

                    Regex m_RegTex = new Regex(@"\\hspace\{([^\{^\}]*)\}", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                    strTmpTex = m_RegTex.Replace(strTmpTex, "");

                    sDicPic.Add(x.ToString(), strTmpTex);
                }
            }

            strTmpDoc = Application.StartupPath + "\\temp\\" + DateTime.Now.Ticks.ToString() + ".docx";

            HtmlToWord(strTmpDoc, sHtmlText);

            if (sDicPic.Count > 0)
            {
                try
                {

                    //ConvertEq p = new ConvertEq();
                    //ConvertEquation ce = new ConvertEquation();

                    // Add the OfficeMath instance to our word template.
                    using (WordprocessingDocument wordDoc =
                      WordprocessingDocument.Open(strTmpDoc, true))
                    {

                        var body = wordDoc.MainDocumentPart.Document.Body;
                        var paras = body.Elements<Paragraph>();

                        int j = 0;
                        foreach (var para in paras)
                        {
                            foreach (var run in para.Elements<Run>())
                            {
                                foreach (var OpenXmlElement in run.Elements<Drawing>())
                                {
                                    j = j + 1;

                                    string strLatex = "";
                                    if (sDicPic.ContainsKey(j.ToString()))
                                    {
                                        strLatex = sDicPic[j.ToString()];
                                    }

                                    if (string.IsNullOrEmpty(strLatex) == false)
                                    {
                                        //string strMathFile = p.GetOutputFile("txt");

                                        //由于此方法会造成mathtype服务器报窗口打开太多的错误，因此废止，2021-09-14
                                        //ce.Convert(new EquationInputFileText2(strLatex, ClipboardFormats.cfTeX),
                                        //    new EquationOutputFileText(strMathFile, "MathJax-MathML.tdl"));     

                                        //(<math[^>])([\s\S]*?)(</math>)
                                        //改用调用网页的形式用正则表达式获取转换后的xml代码

                                        //string strTmpXml = readTxt(strMathFile);

                                        //用网页形式获取的xml代码不需要再更改
                                        //if (string.IsNullOrEmpty(strTmpXml) == false)
                                        //{
                                        //    strTmpXml = strTmpXml.Replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
                                        //}

                                        string strTmpXml = GetWebMathMml(strLatex);

                                        if (string.IsNullOrEmpty(strTmpXml) == false)
                                        {

                                            XmlDocument xDoc = new XmlDocument();
                                            xDoc.LoadXml(strTmpXml);

                                            XmlNode xErrNode = null;

                                            try
                                            {
                                                xErrNode = xDoc.ChildNodes[0].ChildNodes[0];	//xDoc.SelectSingleNode("math/merror");
                                            }
                                            catch
                                            {

                                            }

                                            if (xErrNode == null || xErrNode.Name != "merror")
                                            {
                                                XslCompiledTransform xslTransform = new XslCompiledTransform();

                                                // The MML2OMML.xsl file is located under 
                                                // %ProgramFiles%\Microsoft Office\Office12\
                                                xslTransform.Load("xsltran\\MML2OMML.xsl");

                                                // Load the file containing your MathML presentation markup.
                                                using (XmlReader reader = new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)))
                                                {
                                                    using (MemoryStream ms = new MemoryStream())
                                                    {
                                                        XmlWriterSettings settings = xslTransform.OutputSettings.Clone();

                                                        // Configure xml writer to omit xml declaration.
                                                        settings.ConformanceLevel = ConformanceLevel.Fragment;
                                                        settings.OmitXmlDeclaration = true;

                                                        XmlWriter xw = XmlWriter.Create(ms, settings);

                                                        // Transform our MathML to OfficeMathML
                                                        xslTransform.Transform(reader, xw);
                                                        ms.Seek(0, SeekOrigin.Begin);

                                                        StreamReader sr = new StreamReader(ms, Encoding.UTF8);

                                                        string officeML = sr.ReadToEnd();

                                                        Console.Out.WriteLine(officeML);

                                                        // Create a OfficeMath instance from the OfficeMathML xml.
                                                        DocumentFormat.OpenXml.Math.OfficeMath om =
                                                          new DocumentFormat.OpenXml.Math.OfficeMath(officeML);

                                                        //[FIMG_001]
                                                        //text.Text = text.Text.Replace("text-to-replace", "replaced-text");

                                                        foreach (var currentRun in om.Descendants<DocumentFormat.OpenXml.Math.Run>())
                                                        {
                                                            // Add font information to every run.
                                                            DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties2 =
                                                              new DocumentFormat.OpenXml.Wordprocessing.RunProperties();

                                                            RunFonts runFonts2 = new RunFonts() { Ascii = "Cambria Math", HighAnsi = "Cambria Math" };
                                                            runProperties2.Append(runFonts2);

                                                            currentRun.InsertAt(runProperties2, 0);
                                                        }

                                                        OpenXmlElement.Parent.ReplaceChild(om, OpenXmlElement);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                AppendMsg("转换第" + j.ToString() + "个图片公式:" + strLatex + " 失败:" + xErrNode.InnerText + "!\n");
                                            }
                                        }
                                        else
                                        {
                                            AppendMsg("转换第" + j.ToString() + "个图片公式:" + strLatex + " 失败!\n");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show(ex.ToString());
                }

                this.Cursor = Cursors.Default;
                pictureBox1.Visible = false;
                btnOk.Enabled = true;
                button1.Enabled = true;
            }

            //MessageBox.Show("转换完成!具体请查看"+ strDocxFile);
            AppendMsg("转换完成!具体请查看" + strTmpDoc + "\n");

            Process p1 = new Process();
            p1.StartInfo.FileName = "explorer.exe";
            p1.StartInfo.Arguments = @" /open, " + strTmpDoc;
            p1.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            btnOk.Enabled = false;
            button1.Enabled = false;
            pictureBox1.Visible = true;
            this.Cursor = Cursors.WaitCursor;
            
            //读取题库平台导出来的HTML文件，里面包含公式图片
            string sHtmlText = readTxt(Application.StartupPath + "\\test.html");

            int x = 0;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(sHtmlText);

            HtmlNodeCollection imgList = doc.DocumentNode.SelectNodes(".//img");

            if (imgList != null)
            {
                foreach (HtmlNode tmpImg in imgList)
                {
                    x++;

                    HtmlAttribute latex = tmpImg.Attributes["data-latex"];
                    //Console.WriteLine(latex.Value);

                    string strLatex = latex.Value;

                    Regex m_RegTex = new Regex(@"\\hspace\{([^\{^\}]*)\}", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                    strLatex = m_RegTex.Replace(strLatex, "");

                    strLatex = strLatex.Replace("text{}", "");

                    //sDicPic.Add(x.ToString(), strTmpTex);

                    if (string.IsNullOrEmpty(strLatex) == false)
                    {
                        string strTmpFile = Application.StartupPath + "\\temp\\" + DateTime.Now.Ticks.ToString() + ".png";
                        string strWebUrl = "http://localhost:4100/tex?tex=" + UtilHttp.UrlEncode(strLatex) + "&format=png";

                        //string strPngUrl = UtilHttp.HttpGet(strWebUrl);   //UtilHttp.HttpPost(strWebUrl, "tex=" + strLatex);

                        try
                        {
                            string strImgBase64 = UtilHttp.ImageToBase64(strWebUrl);
                            string imgurl = tmpImg.Attributes["src"].Value;

                            sHtmlText = sHtmlText.Replace(latex.Value, strLatex);
                            sHtmlText = sHtmlText.Replace(imgurl, strImgBase64);

                            //Image sImg = UtilHttp.UrlToImage(strWebUrl);
                            //sImg.Save(strTmpFile);
                        }
                        catch
                        {
                            AppendMsg("转换第" + x.ToString() + "个公式:" + strLatex + " 为图片时失败!\n");
                        }
                    }
                }
            }

            strTmpDoc = Application.StartupPath + "\\temp\\" + DateTime.Now.Ticks.ToString() + ".docx";
            string strTmpHtml = Application.StartupPath + "\\temp\\" + DateTime.Now.Ticks.ToString() + ".html";

            System.IO.File.WriteAllText(strTmpHtml, sHtmlText, Encoding.UTF8);

            HtmlToWord(strTmpDoc, sHtmlText);

            AppendMsg("转换完成!具体请查看" + strTmpDoc + "\n");

            Process p1 = new Process();
            p1.StartInfo.FileName = "explorer.exe";
            p1.StartInfo.Arguments = @" /open, " + strTmpDoc;
            p1.Start();

            this.Cursor = Cursors.Default;
            pictureBox1.Visible = false;
            btnOk.Enabled = true;
            button1.Enabled = true;
        }
    }
}

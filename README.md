# 转换Latex为office math公式

#### 介绍
转换网页中的包含Latex代码的图片为office word自带的公式编辑器可用的公式。其中，用到了node.js的mathjax来转换latex为mathmml代码。


#### 使用说明

1、由于使用MathType转换代码为MML时会弹出错误信息：打开窗口太多。而用html页面的MathJax插件无法获取到js执行后的页面代码，因此改用nodejs的程序来转换.

2、nodejs程序使用部署在电脑上的程序，调用地址：http://localhost:3000/tex2mml?tex=\sqrt {{a^2} + {b^2}} *3

3、无法将文本内容替换为图片，所以先把所有公式都替换为了图片，然后代码能顺利转换的最后就把对应的图片替换为office公式。

4、需要自己部署node.js,安装好node.js之后，进入 Node.js command prompt的命令行模式，输入以下指令：
e:\
cd myapp
node app.js
就会进入到myapp所在目录，执行app.js程序，开启一个web服务，端口3000（具体可自己修改，修改完需要重启服务）。



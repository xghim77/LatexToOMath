﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace TestOpenXmlW
{
    //通过WebBrowser获取html页面内容，无法获得js脚本执行后的页面，因此放弃使用
    public class GetFinalHTML
    {
        private String htmlString;
        private String url;
        private String htmlTitle;
        private bool success = false;

        // 获得html title标签的内容
        public String HtmlTitle
        {
            get
            {
                if (success == false) return null;
                return htmlTitle;
            }
        }

        /// 
        /// 获得执行完js之后的网页body 部分的html代码
        /// 
        public String HtmlBody
        {
            get
            {
                if (success == false) return null;
                return htmlString;
            }

        }

        public GetFinalHTML()
        {
            htmlString = "";
            success = false;
        }

        /// 
        /// 检查并补充设置url
        ///
        private void CheckURL(String url)
        {
            if (!url.StartsWith("http://") && !url.StartsWith("https://") && !url.StartsWith("file:///"))
                url = "http://" + url;

            this.url = url;

        }

        /// 
        /// 加载指定文件
        /// 
        /// 文件URL
        /// 超时时限
        /// 能否成功运行，没有超时
        public bool Run(String url, int timeOut = 10000)
        {
            CheckURL(url);

            Thread newThread = new Thread(NewThread);

            newThread.SetApartmentState(ApartmentState.STA);/// 为了创建WebBrowser类的实例 必须将对应线程设为单线程单元

            newThread.Start();

            //监督子线程运行时间

            while (newThread.IsAlive && timeOut > 0)
            {
                Thread.Sleep(100);

                timeOut -= 100;
            }

            // 超时处理

            if (newThread.IsAlive)
            {
                if (success) return true;

                newThread.Abort();

                return false;
            }

            return true;

        }

        private void NewThread()
        {
            new FinalHtmlPerThread(this);

            Application.Run();// 循环等待webBrowser 加载完毕 调用 DocumentCompleted 事件

        }

        /// 
        ///  用于处理一个url的核心类
        /// 
        class FinalHtmlPerThread : IDisposable
        {
            GetFinalHTML master;

            WebBrowser web;

            public FinalHtmlPerThread(GetFinalHTML master)
            {
                this.master = master;

                DealWithUrl();

            }

            private void DealWithUrl()
            {
                String url = master.url;

                web = new WebBrowser();

                bool success = false;

                try
                {
                    web.Url = new Uri(url);

                    web.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(web_DocumentCompleted); // 对事件加委托

                    success = true;

                }
                finally
                {
                    if (!success)

                        Dispose();

                }
            }

            public void Dispose()
            {
                if (!web.IsDisposed)
                    web.Dispose();

            }


            private void web_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
            {
                //微软官方回答 一个网页有多个Ifram元素就有可能触发多次此事件， 并且提到了
                // vb 和 C++ 的解决方案， C# 没有提及， 经本人尝试，发现下面的语句可以判断成功
                // 假如未完全加载 web.ReadyState = WebBrowserReadyState.Interactive
                if (web.ReadyState != WebBrowserReadyState.Complete) return;

                //System.Threading.Thread.Sleep(500);

                master.htmlTitle = web.Document.Title;
                master.htmlString = web.Document.Body.InnerHtml; //web.Document;  //web.Document.Body.InnerHtml;

                master.success = true;

                Thread.CurrentThread.Abort();
            }
        }
    }
}